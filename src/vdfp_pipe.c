/*-
 * Copyright 2019, 2023 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* for strdup() */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "cache/cache.h"
#include "cache/cache_filter.h"
#include "vcl.h"

#include "from_varnish/vfil.h"
#include "from_varnish/vsub.h"

#include "vcc_if.h"

#define VFAIL(ctx, type, fmt, ...)				\
        VRT_fail((ctx), type " pipe failure: " fmt, __VA_ARGS__)

#define VDPFAIL(ctx, fmt, ...)				\
        VFAIL((ctx), "vdp", fmt, __VA_ARGS__)

#ifdef PIPE_BUF
#define DEFAULT_BUFSZ ((size_t)PIPE_BUF)
#else
#define DEFAULT_BUFSZ ((size_t)512)
#endif

struct setenv_entry {
	unsigned		magic;
#define PIPE_SETENV_MAGIC 0x1167db2e
	VSTAILQ_ENTRY(setenv_entry) list;
	char			*var;
	char			*value;
	int			overwrite;
};

VSTAILQ_HEAD(setenv_head, setenv_entry);

struct VPFX(pipe_vdp) {
	unsigned		magic;
#define PIPE_VDP_MAGIC 0xa887d6c3
	char			*name;
	char			*path;
	struct vdp		*vdp;
	char			**argv;
	struct setenv_head	*setenv_head;
	size_t			bufsz;
	int			argc;
	int			tmo_ms;
};

struct vdp_map {
	unsigned		magic;
#define PIPE_VDP_MAP_MAGIC 0x87a66950
	VRBT_ENTRY(vdp_map)	entry;
	struct vcl		*vcl;
	const struct vdp       	*vdp;
	struct VPFX(pipe_vdp)	*obj;
};

static inline int
map_cmp(const struct vdp_map *v1, const struct vdp_map *v2)
{
	if (v1->vcl < v2->vcl)
		return (-1);
	if (v1->vcl > v2->vcl)
		return (1);
	if (v1->vdp == v2->vdp)
		return (0);
	if (v1->vdp < v2->vdp)
		return (-1);
	return (1);
}

VRBT_HEAD(vdp_tree, vdp_map);

VRBT_GENERATE_INSERT_COLOR(vdp_tree, vdp_map, entry, static)
VRBT_GENERATE_FIND(vdp_tree, vdp_map, entry, map_cmp, static)
#ifdef VRBT_GENERATE_INSERT_FINISH
VRBT_GENERATE_INSERT_FINISH(vdp_tree, vdp_map, entry, static)
#endif
VRBT_GENERATE_INSERT(vdp_tree, vdp_map, entry, map_cmp, static)
VRBT_GENERATE_REMOVE_COLOR(vdp_tree, vdp_map, entry, static)
VRBT_GENERATE_REMOVE(vdp_tree, vdp_map, entry, static)
VRBT_GENERATE_NEXT(vdp_tree, vdp_map, entry, static)
VRBT_GENERATE_MINMAX(vdp_tree, vdp_map, entry, static)

static struct vdp_tree tree_h;

struct vdp_state {
	unsigned		magic;
#define PIPE_VDP_STATE_MAGIC 0xaeb87f5f
	struct pollfd		fds[3];
	struct VPFX(pipe_vdp)	*obj;
	char			*buf;
	pid_t			chldpid;
};

static const char * const stream_name[] = {
	[STDIN_FILENO]  = "stdin",
	[STDOUT_FILENO] = "stdout",
	[STDERR_FILENO] = "stderr",
};

struct task_cfg {
	unsigned		magic;
#define PIPE_TASK_MAGIC 0x43294c37
	char			**argv;
	struct setenv_head	*setenv_head;
	int			argc;
};

/* VDP */

static inline int
mk_pipe(int fds[2], char *name, struct vsl_log *vsl)
{
	errno = 0;
	if (pipe(fds) != 0) {
		VSLb(vsl, SLT_Error, "vdfp_pipe: vdp %s: pipe(2) failed: %s",
		     name, VAS_errtxt(errno));
		return (-1);
	}
	return (0);
}

static inline int
mk_dup(int oldfd, int newfd)
{
	errno = 0;
	if (dup2(oldfd, newfd) == -1) {
		fprintf(stderr, "dup2(2) failed for %s: %s", stream_name[newfd],
			VAS_errtxt(errno));
		return (-1);
	}
	return (0);
}

static int v_matchproto_(vdp_init_f)
vdp_init(VRT_CTX, struct vdp_ctx *vc, void **priv)
{
	struct vdp_state *state;
	struct vdp_entry *vdpe;
	struct vdp_map map_entry, *map;
	struct VPFX(pipe_vdp) *obj;
	int in[2], out[2], err[2];
	struct vmod_priv *task_priv;
	struct task_cfg *task;
	struct setenv_head *setenv_head;
	char **argv;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vc, VDP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vc->hp, HTTP_MAGIC);
	AN(vc->clen);

	AN(priv);
	AZ(*priv);

	vdpe = VTAILQ_LAST(&vc->vdp, vdp_entry_s);
	CHECK_OBJ_NOTNULL(vdpe, VDP_ENTRY_MAGIC);
	AN(vdpe->vdp);
	map_entry.vdp = vdpe->vdp;
	map_entry.vcl = ctx->vcl;
	map = VRBT_FIND(vdp_tree, &tree_h, &map_entry);
	CHECK_OBJ_NOTNULL(map, PIPE_VDP_MAP_MAGIC);
	CHECK_OBJ_NOTNULL(map->obj, PIPE_VDP_MAGIC);
	obj = map->obj;
	AN(obj->bufsz);

	errno = 0;
	ALLOC_OBJ(state, PIPE_VDP_STATE_MAGIC);
	if (state == NULL) {
		VSLb(vc->vsl, SLT_Error, "vdfp_pipe: vdp %s: cannot allocate "
		     "state: %s", obj->name, VAS_errtxt(errno));
		return (-1);
	}
	state->buf = malloc(obj->bufsz);
	if (state->buf == NULL) {
		VSLb(vc->vsl, SLT_Error, "vdfp_pipe: vdp %s: cannot allocate "
		     "buffer size %zd: %s", obj->name, obj->bufsz,
		     VAS_errtxt(errno));
		FREE_OBJ(state);
		return (-1);
	}
	*priv = state;
	state->obj = obj;
	state->chldpid = -1;
	state->fds[STDIN_FILENO].events = POLLOUT;
	state->fds[STDOUT_FILENO].events = POLLIN;
	state->fds[STDERR_FILENO].events = POLLIN;

	argv = obj->argv;
	setenv_head = obj->setenv_head;

	task_priv = VRT_priv_task(ctx, obj);
	AN(priv);
	if (task_priv->priv != NULL) {
		AN(WS_Allocated(ctx->ws, task_priv->priv, sizeof(*task)));
		CAST_OBJ_NOTNULL(task, task_priv->priv, PIPE_TASK_MAGIC);
		if (task->argv != NULL)
			argv = task->argv;
		if (task->setenv_head != NULL)
			setenv_head = task->setenv_head;
	}

	if (mk_pipe(in, obj->name, vc->vsl) != 0)
		return (-1);
	if (mk_pipe(out, obj->name, vc->vsl) != 0)
		return (-1);
	if (mk_pipe(err, obj->name, vc->vsl) != 0)
		return (-1);

	errno = 0;
	state->chldpid = fork();
	if (state->chldpid < 0) {
		VSLb(vc->vsl, SLT_Error, "vdfp_pipe: vdp %s: fork failed for "
		     "%s: %s", obj->name, obj->path, VAS_errtxt(errno));
		closefd(&in[0]);
		closefd(&in[1]);
		closefd(&out[0]);
		closefd(&out[1]);
		closefd(&err[0]);
		closefd(&err[1]);
		return (-1);
	}

	if (state->chldpid == 0) {
		struct setenv_entry *setenv_entry;

		if (mk_dup(err[1], STDERR_FILENO) != 0)
			exit(EXIT_FAILURE);
		if (mk_dup(in[0], STDIN_FILENO) != 0)
			exit(EXIT_FAILURE);
		if (mk_dup(out[1], STDOUT_FILENO) != 0)
			exit(EXIT_FAILURE);

		VSUB_closefrom(STDERR_FILENO + 1);

		if (setenv_head != NULL)
			VSTAILQ_FOREACH(setenv_entry, setenv_head, list) {
				CHECK_OBJ_NOTNULL(setenv_entry,
						  PIPE_SETENV_MAGIC);
				errno = 0;
				if (setenv(setenv_entry->var,
					   setenv_entry->value,
					   setenv_entry->overwrite) != 0) {
					fprintf(stderr,	"Cannot set environment"
						" variable %s=%s: %s",
						setenv_entry->var,
						setenv_entry->value,
						VAS_errtxt(errno));
					exit(EXIT_FAILURE);
				}
			}

		errno = 0;
		if (execve(obj->path, argv, environ) == -1) {
			fprintf(stderr, "cannot exec %s: %s", obj->path,
				VAS_errtxt(errno));
			exit(EXIT_FAILURE);
		}
	}

	VSLb(vc->vsl, SLT_Notice, "vdfp_pipe: vdp %s: exec'd %s as pid %jd",
	     obj->name, obj->path, (intmax_t)state->chldpid);
	closefd(&in[0]);
	closefd(&out[1]);
	closefd(&err[1]);
	(void) VFIL_nonblocking(in[1]);
	state->fds[STDIN_FILENO].fd = in[1];
	state->fds[STDOUT_FILENO].fd = out[0];
	state->fds[STDERR_FILENO].fd = err[0];
	http_Unset(vc->hp, H_Content_Length);
	return (0);
}

static inline void
close_all(struct pollfd *fds)
{
	for (int i = 0; i < 3; i++)
		if (fds[i].fd != -1)
			closefd(&fds[i].fd);
}

/* Assumes CHECK_OBJ* for state called by caller. */
static int
check_pid(struct vdp_state *state, struct vsl_log *vsl, int options)
{
	int status;
	pid_t pid;

	AN(vsl);
	assert(state->chldpid > 0);

	errno = 0;
	while ((pid = waitpid(state->chldpid, &status, options)) < 0) {
		assert(errno == EINTR);
		continue;
	}
	if (pid == 0) {
		AN(options & WNOHANG);
		return (0);
	}
	assert(pid == state->chldpid);
	state->chldpid = -1;
	close_all(state->fds);
	if (WIFEXITED(status)) {
		if (WEXITSTATUS(status) == 0) {
			VSLb(vsl, SLT_Notice, "vdfp_pipe: vdp %s: %s exited "
			     "with status 0", state->obj->name,
			     state->obj->path);
			return (1);
		}
		VSLb(vsl, SLT_Error, "vdfp_pipe: vdp %s: %s exited with status "
		     "%d", state->obj->name, state->obj->path,
		     WEXITSTATUS(status));
		return (-1);
	}
	if (WIFSIGNALED(status)) {
		VSLb(vsl, SLT_Error, "vdfp_pipe: vdp %s: %s terminated with "
		     "signal %d (%s)", state->obj->name, state->obj->path,
		     WTERMSIG(status), strsignal(WTERMSIG(status)));
#ifdef WCOREDUMP
		if (WCOREDUMP(status))
			VSLb(vsl, SLT_Error, "vdfp_pipe: vdp %s: %s dumped "
			     "core", state->obj->name, state->obj->path);
#endif
		return (-1);
	}
	/* XXX assuming no ptrace/WIFSTOPPED/WIFCONTINUED etc */
	return (-1);
}

static int v_matchproto_(vdp_bytes_f)
vdp_bytes(struct vdp_ctx *ctx, enum vdp_action act, void **priv,
	  const void *ptra, ssize_t len)
{
	struct vdp_state *state;
	struct pollfd *fds;
	int retval;
	ssize_t nbytes;
	struct VPFX(pipe_vdp) *obj;
	enum vdp_action act_bytes = VDP_FLUSH;
	const char *ptr;

	CHECK_OBJ_NOTNULL(ctx, VDP_CTX_MAGIC);
	ptr = ptra;
	assert(len >= 0);
	AN(priv);
	CAST_OBJ_NOTNULL(state, *priv, PIPE_VDP_STATE_MAGIC);
	CHECK_OBJ_NOTNULL(state->obj, PIPE_VDP_MAGIC);
	obj = state->obj;
	AN(obj->name);
	AN(obj->path);
	AN(obj->bufsz);
	fds = state->fds;

	for (;;) {
		fds[STDIN_FILENO].revents = 0;
		fds[STDOUT_FILENO].revents = 0;
		fds[STDERR_FILENO].revents = 0;

		if (len == 0 && act == VDP_END && fds[STDIN_FILENO].fd != -1)
			closefd(&fds[STDIN_FILENO].fd);

		errno = 0;
		retval = poll(fds, (nfds_t)3, obj->tmo_ms);
		if (retval < 0) {
			assert(errno == EINTR);
			continue;
		}
		if (retval == 0) {
			if (obj->tmo_ms == 0)
				continue;
			VSLb(ctx->vsl, SLT_Error, "vdfp_pipe: vdp %s: timeout "
			     "waiting for %s", obj->name, obj->path);
			return (-1);
		}

		for (int i = 0; i < 3; i++) {
			if (fds[i].revents == 0)
				continue;
			AZ(fds[i].revents & POLLNVAL);
			if (fds[i].revents & POLLERR) {
				VSLb(ctx->vsl, SLT_Error, "vdfp_pipe: vdp %s: "
				     "error polling %s %s", obj->name,
				     obj->path, stream_name[i]);
				close_all(fds);
				return (-1);
			}
			if (fds[i].revents & POLLHUP && i == STDIN_FILENO) {
				closefd(&fds[STDIN_FILENO].fd);
				continue;
			}
			if (fds[i].revents & POLLOUT) {
				assert(i == STDIN_FILENO);
				if (len == 0 || ptr == NULL)
					continue;

				errno = 0;
				nbytes = write(fds[STDIN_FILENO].fd,
					       ptr, (size_t)len);
				if (nbytes < 0) {
					VSLb(ctx->vsl, SLT_Error,
					     "vdfp_pipe: vdp %s: error writing "
					     "to %s stdin: %s", obj->name,
					     obj->path, VAS_errtxt(errno));
					close_all(fds);
					return (-1);
				}
				AN(nbytes);
				assert(nbytes <= len);
				len -= nbytes;
				ptr += nbytes;
				continue;
			}
			assert(i == STDOUT_FILENO || i == STDERR_FILENO);
			if (!(fds[i].revents & POLLIN)) {
				AN(fds[i].revents & POLLHUP);
				if (i == STDERR_FILENO) {
					closefd(&fds[STDERR_FILENO].fd);
					continue;
				}
				closefd(&fds[STDOUT_FILENO].fd);
				if (act_bytes != VDP_END)
					(void) VDP_bytes(ctx, VDP_END,
						  NULL, (size_t)0);
				continue;
			}
			errno = 0;
			nbytes = read(fds[i].fd, state->buf, obj->bufsz);

			if (nbytes < 0) {
				VSLb(ctx->vsl, SLT_Error, "vdfp_pipe: vdp %s:"
				     " error reading %s from %s: %s",
				     obj->name, stream_name[i], obj->path,
				     VAS_errtxt(errno));
				close_all(fds);
				return (-1);
			}
			assert(nbytes > 0);
			if (i == STDOUT_FILENO &&
			    fds[i].revents & POLLHUP &&
			    (size_t)nbytes < obj->bufsz) {
				act_bytes = VDP_END;
			}
			if (i == STDOUT_FILENO) {
				retval = VDP_bytes(ctx, act_bytes, state->buf,
						   nbytes);
				if (retval < 0) {
					close_all(fds);
					return (-1);
				}
				continue;
			}

			/* Log the stderr message one line at a time. */
			for (char *errmsg = state->buf; nbytes > 0;) {
				size_t linelen;
				char *newline =
					memchr(errmsg, '\n', (size_t)nbytes);
				if (newline == NULL)
					linelen = (size_t)nbytes;
				else
					linelen = (size_t)(newline - errmsg);
				assert(linelen <= (unsigned)nbytes);
				if (linelen == 0) {
					nbytes--;
					errmsg++;
					continue;
				}
				VSLb(ctx->vsl, SLT_Error,
				     "vdfp_pipe: vdp %s: %s stderr: %.*s",
				     obj->name, obj->path, (int)linelen,
				     errmsg);
				linelen++;
				nbytes -= (ssize_t)linelen;
				errmsg += linelen;
			}
		}
		if (act == VDP_END && fds[STDOUT_FILENO].fd != -1)
			continue;

		if (len == 0)
			break;
	}
	AZ(len);
	return (0);
}

static int v_matchproto_(vdp_fini_f)
vdp_fini(struct vdp_ctx *ctx, void **priv)
{
	struct vdp_state *state;

	CHECK_OBJ_NOTNULL(ctx, VDP_CTX_MAGIC);
	AN(priv);
	if (*priv == NULL)
		return (0);
	TAKE_OBJ_NOTNULL(state, priv, PIPE_VDP_STATE_MAGIC);
	CHECK_OBJ_NOTNULL(state->obj, PIPE_VDP_MAGIC);
	AN(state->obj->name);
	AN(state->obj->path);

	close_all(state->fds);

	if (state->chldpid != -1)
		(void)check_pid(state, ctx->vsl, 0);
	if (state->buf != NULL)
		free(state->buf);
	FREE_OBJ(state);
	*priv = NULL;
	return (0);
}

/* Event function */

int
vmod_event(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	struct vdp_map *map, *prev_map;

	ASSERT_CLI();
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(priv);

	switch(e) {
	case VCL_EVENT_DISCARD:
		AN(ctx->vcl);
		map = VRBT_MIN(vdp_tree, &tree_h);
		while (map != NULL) {
			CHECK_OBJ(map, PIPE_VDP_MAP_MAGIC);
			prev_map = NULL;
			if (map->vcl == ctx->vcl && map->vdp != NULL) {
				VRT_RemoveFilter(ctx, NULL, map->vdp);
				free(TRUST_ME(map->vdp));
				map->vdp = NULL;
				prev_map = map;
			}
			map = VRBT_NEXT(vdp_tree, &tree_h, map);
			if (prev_map != NULL) {
				AN(VRBT_REMOVE(vdp_tree, &tree_h, prev_map));
				FREE_OBJ(prev_map);
			}
		}
		return (0);
	case VCL_EVENT_LOAD:
	case VCL_EVENT_WARM:
	case VCL_EVENT_COLD:
		return (0);
	default:
		WRONG("illegal event enum");
	}
	NEEDLESS(return (0));
}

static void
vdp_obj_free(VRT_CTX, struct VPFX(pipe_vdp) **vdp_objp)
{
	struct VPFX(pipe_vdp) *vdp_obj;

	TAKE_OBJ_NOTNULL(vdp_obj, vdp_objp, PIPE_VDP_MAGIC);
	free(vdp_obj->argv);
	if (ctx != NULL && vdp_obj->vdp != NULL)
		VRT_RemoveFilter(ctx, NULL, vdp_obj->vdp);
	free(TRUST_ME(vdp_obj->vdp->name));
	free(vdp_obj->vdp);
	free(vdp_obj->path);
	free(vdp_obj->name);
	FREE_OBJ(vdp_obj);
}

/* vdp object */

VCL_VOID
vmod_vdp__init(VRT_CTX, struct VPFX(pipe_vdp) **vdpp, const char *obj_name,
	       VCL_STRING path, VCL_STRING vdp_name, VCL_BYTES bufsz,
	       VCL_DURATION timeout)
{
	struct VPFX(pipe_vdp) *vdp_obj;
	struct vdp *vdp;
	struct vdp_map *map;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(vdpp);
	AZ(*vdpp);
	AN(obj_name);

	if (path == NULL || *path == '\0') {
		VDPFAIL(ctx, "new %s: path is empty", obj_name);
		return;
	}
	errno = 0;
	if (access(path, X_OK) != 0) {
		VDPFAIL(ctx, "new %s: cannot execute %s: %s", obj_name, path,
			VAS_errtxt(errno));
		return;
	}
	if (vdp_name == NULL || *vdp_name == '\0')
		vdp_name = obj_name;

#define CHK_NAME(nm) do {						\
		if (strcmp(vdp_name, (nm)) == 0) {			\
			VDPFAIL(ctx, "new %s: filter name %s already in use " \
				"by another VDP", obj_name, vdp_name);	\
			return;						\
		}							\
	} while(0)

	CHK_NAME("esi");
	CHK_NAME("gunzip");
	CHK_NAME("range");
	CHK_NAME("VED");
	CHK_NAME("PGZ");
	CHK_NAME("VZZ");
	CHK_NAME("V1B");
	CHK_NAME("H2B");

	if (bufsz <= 0)
		bufsz = DEFAULT_BUFSZ;

	errno = 0;
	ALLOC_OBJ(vdp_obj, PIPE_VDP_MAGIC);
	if (vdp_obj == NULL) {
		VDPFAIL(ctx, "new %s: cannot allocate space for object: %s",
			obj_name, VAS_errtxt(errno));
		return;
	}
	vdp_obj->name = strdup(obj_name);
	vdp_obj->path = strdup(path);
	vdp_obj->bufsz = (size_t)bufsz;
	vdp_obj->tmo_ms = (int)(timeout * 1000);
	AZ(vdp_obj->setenv_head);

	errno = 0;
	vdp = malloc(sizeof(*vdp));
	if (vdp == NULL) {
		VDPFAIL(ctx, "new %s: cannot allocate space for VDP: %s",
			obj_name, VAS_errtxt(errno));
		vdp_obj_free(NULL, &vdp_obj);
		return;
	}
	vdp->name = strdup(vdp_name);
	vdp->init = vdp_init;
	vdp->bytes = vdp_bytes;
	vdp->fini = vdp_fini;

	vdp_obj->vdp = vdp;

	if (VRT_AddFilter(ctx, NULL, vdp)) {
		vdp_obj_free(NULL, &vdp_obj);
		return;
	}

	errno = 0;
	ALLOC_OBJ(map, PIPE_VDP_MAP_MAGIC);
	if (map == NULL) {
		VDPFAIL(ctx, "new %s: cannot allocate space for map entry: %s",
			obj_name, VAS_errtxt(errno));
		vdp_obj_free(ctx, &vdp_obj);
		return;
	}

	errno = 0;
	vdp_obj->argv = malloc(2 * sizeof(*vdp_obj->argv));
	if (vdp_obj->argv == NULL) {
		VDPFAIL(ctx, "new %s: cannot allocate argv: %s", obj_name,
			VAS_errtxt(errno));
		FREE_OBJ(map);
		vdp_obj_free(ctx, &vdp_obj);
		return;
	}
	vdp_obj->argv[0] = vdp_obj->path;
	vdp_obj->argv[1] = NULL;
	vdp_obj->argc = 1;

	map->vdp = vdp;
	map->vcl = ctx->vcl;
	map->obj = vdp_obj;
	AZ(VRBT_INSERT(vdp_tree, &tree_h, map));

	*vdpp = vdp_obj;
	//lint -e{429} Flexelint does not grok map insert
	return;
}

/* Event function calls VRT_RemoveFilter, since it needs a VRT_CTX. */

VCL_VOID
vmod_vdp__fini(struct VPFX(pipe_vdp) **vdpp)
{
	struct VPFX(pipe_vdp) *vdp_obj;
	struct setenv_entry *setenv_entry;

	if (vdpp == NULL || *vdpp == NULL)
		return;
	TAKE_OBJ_NOTNULL(vdp_obj, vdpp, PIPE_VDP_MAGIC);
	if (vdp_obj->name != NULL)
		free(vdp_obj->name);
	if (vdp_obj->path != NULL)
		free(vdp_obj->path);
	if (vdp_obj->argv != NULL) {
		for (int i = 1; vdp_obj->argv[i] != NULL; i++)
			free(vdp_obj->argv[i]);
		free(vdp_obj->argv);
	}
	if (vdp_obj->setenv_head != NULL)
		while (!VSTAILQ_EMPTY(vdp_obj->setenv_head)) {
			setenv_entry = VSTAILQ_FIRST(vdp_obj->setenv_head);
			CHECK_OBJ_NOTNULL(setenv_entry, PIPE_SETENV_MAGIC);
			if (setenv_entry->var != NULL)
				free(setenv_entry->var);
			if (setenv_entry->value != NULL)
				free(setenv_entry->value);
			VSTAILQ_REMOVE_HEAD(vdp_obj->setenv_head, list);
			FREE_OBJ(setenv_entry);
		}
	FREE_OBJ(vdp_obj);
}

static void
task_free(VRT_CTX, void *p)
{
	struct task_cfg *task;
	(void)ctx;

	CAST_OBJ_NOTNULL(task, p, PIPE_TASK_MAGIC);
	if (task->argv != NULL)
		free(task->argv);
}

static const struct vmod_priv_methods priv_task_methods[1] = {{
		.magic = VMOD_PRIV_METHODS_MAGIC,
		.type = "vmod_pipe_priv_task",
		.fini = task_free
}};

static struct task_cfg *
get_task(VRT_CTX, struct VPFX(pipe_vdp) *obj, const char *method)
{
	struct vmod_priv *priv;
	struct task_cfg *task;

	priv = VRT_priv_task(ctx, obj);
	AN(priv);
	if (priv->priv == NULL) {
		WS_TASK_ALLOC_OBJ(ctx, task, PIPE_TASK_MAGIC);
		if (task == NULL) {
			VDPFAIL(ctx, "%s.%s(): insufficient workspace for "
				"task config", obj->name, method);
			return (NULL);
		}
		priv->len = sizeof(*task);
		priv->methods = priv_task_methods;
		priv->priv = task;
	}
	else {
		AN(WS_Allocated(ctx->ws, priv->priv, sizeof(*task)));
		CAST_OBJ_NOTNULL(task, priv->priv, PIPE_TASK_MAGIC);
	}
	return (task);
}

VCL_VOID
vmod_vdp_arg(VRT_CTX, struct VPFX(pipe_vdp) *obj, VCL_STRING arg)
{
	struct task_cfg *task;
	size_t sz;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	CHECK_OBJ_NOTNULL(obj, PIPE_VDP_MAGIC);

	AN(ctx->method & (VCL_MET_INIT | VCL_MET_DELIVER));

	if (arg == NULL) {
		VDPFAIL(ctx, "%s.arg(): arg is NULL", obj->name);
		return;
	}

	if (ctx->method & VCL_MET_INIT) {
		errno = 0;
		assert(obj->argc >= 0);
		assert(obj->argc < INT_MAX);

		sz = (unsigned)obj->argc;
		sz += 2;
		sz *= sizeof *obj->argv;
		obj->argv = realloc(obj->argv, sz);
		if (obj->argv == NULL) {
			VDPFAIL(ctx, "%s.arg(): cannot re-allocate argv: %s",
				obj->name, VAS_errtxt(errno));
			return;
		}
		obj->argv[obj->argc++] = strdup(arg);
		obj->argv[obj->argc] = NULL;
		return;
	}

	AN(ctx->method & VCL_MET_DELIVER);
	if ((task = get_task(ctx, obj, "arg")) == NULL)
		return;
	if (task->argv == NULL) {
		errno = 0;
		task->argv = malloc(3 * sizeof(*task->argv));
		if (task->argv == NULL) {
			VDPFAIL(ctx, "%s.arg(): cannot allocate argv: %s",
				obj->name, VAS_errtxt(errno));
			return;
		}
		if ((task->argv[1] = WS_Copy(ctx->ws, arg, -1)) == NULL) {
			VDPFAIL(ctx, "%s.arg(): insufficient workspace for %s",
				obj->name, arg);
			return;
		}
		task->argv[0] = obj->path;
		task->argv[2] = NULL;
		task->argc = 2;
		return;
	}

	assert(task->argc >= 0);
	assert(task->argc < INT_MAX);

	sz = (unsigned)task->argc;
	sz += 2;
	sz *= sizeof *task->argv;

	errno = 0;
	task->argv = realloc(task->argv, sz);
	if (task->argv == NULL) {
		VDPFAIL(ctx, "%s.arg(): cannot re-allocate argv: %s", obj->name,
			VAS_errtxt(errno));
		return;
	}
	if ((task->argv[task->argc++] = WS_Copy(ctx->ws, arg, -1)) == NULL) {
		VDPFAIL(ctx, "%s.arg(): insufficient workspace for %s",
			obj->name, arg);
		return;
	}
	task->argv[task->argc] = NULL;

	return;
}

VCL_VOID
vmod_vdp_setenv(VRT_CTX, struct VPFX(pipe_vdp) *obj, VCL_STRING var,
		VCL_STRING value, VCL_BOOL overwrite)
{
	struct setenv_entry *entry;
	struct task_cfg *task;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	CHECK_OBJ_NOTNULL(obj, PIPE_VDP_MAGIC);

	AN(ctx->method & (VCL_MET_INIT | VCL_MET_DELIVER));

	if (var == NULL || *var == '\0') {
		VDPFAIL(ctx, "%s.setenv(): var is empty", obj->name);
		return;
	}
	if (strchr(var, '=') != NULL) {
		VDPFAIL(ctx, "%s.setenv(): var may not contain '=': %s",
			obj->name, var);
		return;
	}
	if (value == NULL) {
		VDPFAIL(ctx, "%s.setenv(): value is NULL", obj->name);
		return;
	}

	if (ctx->method & VCL_MET_INIT) {
		if (obj->setenv_head == NULL) {
			errno = 0;
			obj->setenv_head = malloc(sizeof(*obj->setenv_head));
			if (obj->setenv_head == NULL) {
				VDPFAIL(ctx, "%s.setenv(): cannot allocate "
					"list head: %s", obj->name,
					VAS_errtxt(errno));
				return;
			}
			VSTAILQ_INIT(obj->setenv_head);
		}

		errno = 0;
		ALLOC_OBJ(entry, PIPE_SETENV_MAGIC);
		if (entry == NULL) {
			VDPFAIL(ctx,
				"%s.setenv(): cannot allocate list entry: %s",
				obj->name, VAS_errtxt(errno));
			return;
		}

		entry->var = strdup(var);
		entry->value = strdup(value);
		entry->overwrite = overwrite ? 1 : 0;
		VSTAILQ_INSERT_TAIL(obj->setenv_head, entry, list);
		return;
	}

	AN(ctx->method & VCL_MET_DELIVER);
	if ((task = get_task(ctx, obj, "setenv")) == NULL)
		return;
	if (task->setenv_head == NULL) {
		task->setenv_head = WS_Alloc(ctx->ws,
					     sizeof(*task->setenv_head));
		if (task->setenv_head == NULL) {
			VDPFAIL(ctx, "%s.setenv(): insufficient workspace for "
				"list head", obj->name);
			return;
		}
		VSTAILQ_INIT(task->setenv_head);
	}

	WS_TASK_ALLOC_OBJ(ctx, entry, PIPE_SETENV_MAGIC);
	if (entry == NULL) {
		VDPFAIL(ctx, "%s.setenv(): insufficient workspace for list "
			"entry", obj->name);
		return;
	}
	if ((entry->var = WS_Copy(ctx->ws, var, -1)) == NULL) {
		VDPFAIL(ctx, "%s.setenv(): insufficient workspace for var",
			obj->name);
		return;
	}
	if ((entry->value = WS_Copy(ctx->ws, value, -1)) == NULL) {
		VDPFAIL(ctx, "%s.setenv(): insufficient workspace for value",
			obj->name);
		return;
	}
	entry->overwrite = overwrite ? 1 : 0;
	VSTAILQ_INSERT_TAIL(task->setenv_head, entry, list);
}

VCL_STRING
vmod_version(VRT_CTX)
{
	(void) ctx;
	return VERSION;
}
