/*-
 * Copyright (c) 2006 Verdens Gang AS
 * Copyright (c) 2006-2011 Varnish Software AS
 * All rights reserved.
 *
 * Author: Poul-Henning Kamp <phk@phk.freebsd.dk>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Run stuff in a child process
 */

#include "config.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>		// Solaris closefrom(3c)
#include <string.h>
#include <unistd.h>		// BSD/Linux close_range(2)
#ifdef HAVE_WORKING_CLOSE_RANGE
#elif HAVE_CLOSEFROM
#else
#  include <dirent.h>
#endif

#include "vdef.h"

#include "vapi/vsig.h"

#include "vas.h"
#include "vfil.h"
#include "vsb.h"
#include "vsub.h"

struct vsub_priv {
	const char	*name;
	struct vsb	*sb;
	int		lines;
	int		maxlines;
};

void
VSUB_closefrom(int fd)
{

	assert(fd >= 0);

#ifdef HAVE_WORKING_CLOSE_RANGE
	AZ(close_range(fd, ~0U, 0));
	return;
#elif HAVE_CLOSEFROM
	closefrom(fd);
	return;
#else
	char buf[128];
	int i, maxfd = 0;
	DIR *d;
	struct dirent *de;
	char *p;

	bprintf(buf, "/proc/%d/fd/", getpid());
	d = opendir(buf);
	if (d != NULL) {
		while (1) {
			de = readdir(d);
			if (de == NULL)
				break;
			i = strtoul(de->d_name, &p, 10);
			if (*p != '\0')
				continue;
			if (i > maxfd)
				maxfd = i;
		}
		AZ(closedir(d));
	}

	if (maxfd == 0)
		maxfd = sysconf(_SC_OPEN_MAX);
	assert(maxfd > 0);
	for (; maxfd > fd; maxfd--)
		(void)close(maxfd);
#endif
}

