AC_PREREQ([2.69])
AC_COPYRIGHT([Copyright 2019 - 2024 UPLEX - Nils Goroll Systemoptimierung])
AC_INIT([libvdfp-pipe],[trunk],[varnish-support@uplex.de],[vdfp-pipe])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_SRCDIR(src/vdfp_pipe.vcc)
AC_CONFIG_HEADERS([config.h])
AC_CANONICAL_TARGET
AC_GNU_SOURCE
AC_LANG(C)

AM_INIT_AUTOMAKE([1.12 -Wall -Werror foreign parallel-tests])
AM_SILENT_RULES([yes])
AM_PROG_AR

LT_PREREQ([2.2.6])
LT_INIT([dlopen disable-static])

# To get absolute paths in the vtc tests.
AC_PATH_PROG([CAT], [cat], [])
AC_PATH_PROG([TR], [tr], [])
AC_PATH_PROG([SED], [sed], [])
AC_PATH_PROG([ENV], [env], [])

AX_PTHREAD(,[AC_MSG_ERROR([Could not configure pthreads support])])

LIBS="$PTHREAD_LIBS $LIBS"
CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
CC="$PTHREAD_CC"

AC_ARG_WITH([rst2man],
	AS_HELP_STRING(
		[--with-rst2man=PATH],
		[Location of rst2man (auto)]),
	[RST2MAN="$withval"],
	[AC_CHECK_PROGS(RST2MAN, [rst2man rst2man.py], [])])
AM_CONDITIONAL(HAVE_RST2MAN, [test "x$RST2MAN" != "xno"])

AC_ARG_WITH([lcov],
	AS_HELP_STRING(
		[--with-lcov=PATH],
		[Location of lcov to generate coverage data (auto)]),
	[LCOV="$withval"],
	[AC_CHECK_PROGS(LCOV, [lcov], [])])
AM_CONDITIONAL(HAVE_LCOV, [test -n "$LCOV"])

AC_ARG_WITH([genhtml],
	AS_HELP_STRING(
		[--with-genhtml=PATH],
		[Location of genhtml to generate coverage reports (auto)]),
	[GENHTML="$withval"],
	[AC_CHECK_PROGS(GENHTML, [genhtml], [])])
AM_CONDITIONAL(HAVE_GENHTML, [test -n "$GENHTML"])

m4_ifndef([VARNISH_PREREQ], AC_MSG_ERROR([Need varnish.m4 -- see README.rst]))

VARNISH_PREREQ([7.5],[trunk])
VARNISH_VMODS([pipe])

VMOD_TESTS="$(cd $srcdir/src && echo tests/*.vtc)"
AC_SUBST(VMOD_TESTS)

PKG_CHECK_VAR([LIBVARNISHAPI_LIBDIR], [varnishapi], [libdir])
AC_SUBST([VARNISH_LIBRARY_PATH],
	[$LIBVARNISHAPI_LIBDIR:$LIBVARNISHAPI_LIBDIR/varnish])

# Checks for C sources
AC_CHECK_FUNCS([dup2])
AC_CHECK_FUNCS([memchr])
AC_CHECK_FUNCS([strdup])
AC_CHECK_FUNCS([closefrom])

AC_CHECK_HEADERS([limits.h])
AC_C_INLINE
AC_FUNC_FORK
AC_FUNC_MALLOC
AC_FUNC_REALLOC
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_TYPE_SSIZE_T

LIBS="${save_LIBS}"

AC_CHECK_FUNCS([close_range])

# Check for working close_range()
if test "$ac_cv_func_close_range" = yes; then
AC_CACHE_CHECK([if close_range is working],
  [ac_cv_have_working_close_range],
  [AC_RUN_IFELSE(
    [AC_LANG_PROGRAM([[
#include <unistd.h>
    ]],[[
return (close_range(0, 2, 0));
    ]])],
    [ac_cv_have_working_close_range=yes],
    [ac_cv_have_working_close_range=no])
  ])
fi
if test "x$ac_cv_have_working_close_range" = xyes; then
   AC_DEFINE([HAVE_WORKING_CLOSE_RANGE], [1],
	     [Define if OS has working close_range()])
fi

# --enable-stack-protector
AC_ARG_ENABLE(stack-protector,
	AS_HELP_STRING([--enable-stack-protector],[enable stack protector (default is YES)]),
	[],
	[enable_stack_protector=yes])

if test "x$enable_stack_protector" != "xno"; then
	AX_CHECK_COMPILE_FLAG([-fstack-protector],
		AX_CHECK_LINK_FLAG([-fstack-protector],
			[CFLAGS="${CFLAGS} -fstack-protector"], [], []),
		[], [])
fi

# --enable-debugging
AC_ARG_ENABLE(debugging,
	AS_HELP_STRING([--enable-debugging],[enable debugging (default is NO)]),
	[],
	[enable_debugging=no])

# AC_PROG_CC sets CFLAGS to '-g -O2' unless already set, so there's no
# need to add -g. Disable or change by explicitly setting CFLAGS. If
# this option is enabled, then -Og or -O0 becomes the last
# optimization option, and hence takes precedence.
if test "x$enable_debugging" != "xno"; then
	CFLAGS="${CFLAGS} -fno-inline"
	AX_CHECK_COMPILE_FLAG([-Og],
		[CFLAGS="${CFLAGS} -O0 -Og"],
		[CFLAGS="${CFLAGS} -O0"],
		[])
fi

AC_CONFIG_FILES([
	Makefile
	src/Makefile
])
AC_OUTPUT
